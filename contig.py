import numpy as np
"""
    Contiguous Subset with Largest Sum

    The goal of this question is to produce, in the language of your choice,
    an algorithm that given a list of integers returns the contiguous subset
    with largest sum.

    For example, if your algorithm is given the list:

     [4, -1, 5, 6, -13, 2]

    Then your algorithm should return:

     [4, -1, 5, 6]
"""

# Brute force solution
def solve_brute(l):
    
    N = len(l)
    
    if (N == 0):
        return []
    
    current_max = float("-inf")
    max_indices = (0, 0)
    
    for start_index in range(N):
        for end_index in range(start_index, N):
            particular_max = sum(l[start_index:end_index + 1])
            if particular_max > current_max:
                #print particular_max, current_max, start_index, end_index
                current_max = particular_max
                max_indices = (start_index, end_index)
    
    return l[max_indices[0]:max_indices[1] + 1]


def solve_fast(l):
    """
    Use dynamic programming. Define A vector at which the A[i] represents the best solution for l[:i + 1]
    """
    N = len(l)
    A = [None] * N
    
    
    for i, e in enumerate(l):
        if i == 0:
            A[0] = l[0]
            continue
        else:
            # Better off continuing if A[i - 1] + e > e
            # Better off starting fresh if A[i - 1] + e < e
            A[i] = max(A[i - 1] + e, e)
    
    # recover the indices
    max_index = np.argmax(np.array(A))
    # recover the min index
    start_index = max_index
    
    for start_index in range(max_index, -1, -1):
        if A[start_index - 1] > 0:
            continue
        else:
            break
            
    return l[start_index:max_index + 1]

print solve_brute([8, -4, -6, 2, 4, -1, 0, 5, -13,  6])
print solve_fast([8, -4, -6, 2, 4, -1, 0, 5, -13,  6])

print solve_brute([4, -1, 5, 6, -13, 2])
print solve_fast([4, -1, 5, 6, -13, 2])


"""

Two parts to this question:

  1. Implement a brute-force solution. What's the running time of
     this solution?

     See solve_brute - The running time is O(n^2).

  2. Implement a more efficient (in time) solution - ideally a solution
     with lowest possible running time. What's the running time of your
     solution? Do you think it's possible to do better? Why or why not?
     
     See solve_fast - The running time is O(n). It's a DP approach that scans the array and records the optimal solution for the subarray up to that point. There is probably not a running time faster than O(n) since every element needs to be inspected. 
     
     
"""