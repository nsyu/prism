import numpy as np
class Image(object):
  
  def __init__(self, x, y, pixel_format, pixel_type, image_buffer = None):
    assert pixel_format in ['float', 'int']
    assert pixel_type in ['grey', 'rgb']
    
    self.width = x
    self.height = y
    
    self.pixel_format = pixel_format
    self.pixel_type = pixel_type
    
    # create data representation
    if self.pixel_format == 'int':
      self.dtype = np.dtype('uint32') 
    elif self.pixel_format == 'float':
      self.dtype = np.dtype('Float64')
      
    if self.pixel_type == 'grey':
      self.depth = 1
    elif self.pixel_type == 'rgb':
      self.depth = 3
      
    self.data = np.zeros(shape = (self.width, self.height, self.depth),
                            dtype = self.dtype)
    
    if image_buffer is not None:
      assert len(image_buffer) == self.width * self.height * self.depth, 'Buffer length incompatible'
      
      buffer_index = 0
      for j in range(self.height):
        for i in range(self.width):
          self.data[i, j, :] = np.array(image_buffer[buffer_index:buffer_index + self.depth])          
          buffer_index += self.depth
    
  def width(self):
    """
    This method should provide the width of the image
    """
    return self.width
  
  def height(self):
    """
    This method should provide the height of the image
    """
    
    return self.height
    
  def copy(self):
    print self.width, self.height, self.pixel_format, self.pixel_type
    image_copy = Image(self.width, self.height, self.pixel_format, self.pixel_type)
    image_copy.data = np.copy(self.data)
    return image_copy
    
  def pixel_get(self, x, y):
    """
    This method should provide pixel access to the image
    """
    return self.data[x, y, :]
  
  def pixel_set(self, x, y, value):
    """
    This method should provide pixel access to the image
    """
    self.data[x, y, :] = np.array(value)
    
  def scale(self, scale_factor):
    """
    Scale: This method should take an integer scale factor and scale the image down (i.e. the resulting image is smaller) by the scale factor in each direction. Write a function stub only, you needn't write the code to do the scaling, but you can if you like. 
    """
    pass
    
  def convolve(self, kernel):
    """
    This method takes an arbitrary kernel of size 3 pixels x 3 pixels, convolves it with the image, and returns a new image that represents the convolved result. Write a function stub only. You needn't write the actual code to do the convolution, but you can if you like.
    """
    pass
    
  def __repr__(self):
    return 'width = %d, height = %d, pixel_format = %s, pixel_type=%s,\n %s)'%(self.width, self.height, self.pixel_format, self.pixel_type, self.data.__repr__())
    
if __name__ == '__main__':
  #image_buffer = [.1, .2, .3, .4]
  #i = Image(2, 2, 'float', 'grey', image_buffer)
  
  image_buffer = [.1,.2, .3, .4,.5, .5, .6,.7, .9, .01,.02, .03]
  i = Image(2, 2, 'float', 'rgb', image_buffer)
  
  print i
  
  
  print 'test copying'
  
  j = i.copy()
  j.pixel_set(0, 0, (.01, .02, .03))
  print i
  print j
  
  """
    Describe your implementation and others you considered. Describe the structure of each class and why it is most appropriate. If you run out of time, describe what you would have liked to do. Explain your approach to the question as well as the upsides and downsides to your choice of language.
    
    I would have liked to have implemented scale and convolve, implemented some more error checking and tests.
    
    I'm not sure if it is possible to to have the accessor do both set and get in python. Not sure how to pass back the array slice by reference rather than by copy
  """
  

  
    